package facci.pm.conforme.marjorie.examen.Actividades;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.conforme.marjorie.examen.Adapter.AdaptadorEstudiantes;
import facci.pm.conforme.marjorie.examen.Modelos.Estudiantes;
import facci.pm.conforme.marjorie.examen.R;

public class Main2Activity extends AppCompatActivity {

    private static String URL_E = "";
    private ArrayList<Estudiantes> estudiantesArrayList;
    private AdaptadorEstudiantes adaptadorEstudiantes;
    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;
    private  String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        id = getIntent().getStringExtra("id");
        estudiantesArrayList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.RecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("CARGANDO");
        progressDialog.show();
        adaptadorEstudiantes = new AdaptadorEstudiantes(estudiantesArrayList);
        URL_E = "http://10.1.15.127:3005/api/estudiantes/";
        Estudiante(URL_E);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.Ascendente:
                URL_E = "http://10.1.15.127:3005/api/estudiantes/asc";
                estudiantesArrayList.clear();
                recyclerView.removeAllViews();
                Estudiante(URL_E);
                return true;
            case R.id.Descendente:
                URL_E = "http://10.1.15.127:3005/api/estudiantes/desc";
                estudiantesArrayList.clear();
                recyclerView.removeAllViews();
                Estudiante(URL_E);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filtrar, menu);
        return true;
    }

    private void Estudiante(String urlE) {
            progressDialog.show();
            StringRequest stringRequest = new StringRequest(Request.Method.GET, urlE, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    progressDialog.dismiss();
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i= 0; i<jsonArray.length(); i++){
                            JSONObject object = jsonArray.getJSONObject(i);
                            Estudiantes estudiantes = new Estudiantes();
                            estudiantes.setId(object.getString("id"));
                            estudiantes.setApellidos(object.getString("apellidos"));
                            estudiantes.setNombres(object.getString("nombres"));
                            estudiantes.setImagen(object.getString("imagen"));
                            estudiantes.setParcial_uno(object.getString("parcial_uno"));
                            estudiantes.setParcial_dos(object.getString("parcial_dos"));
                            estudiantesArrayList.add(estudiantes);
                        }
                        adaptadorEstudiantes = new AdaptadorEstudiantes(estudiantesArrayList);
                        recyclerView.setAdapter(adaptadorEstudiantes);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
    }
}
